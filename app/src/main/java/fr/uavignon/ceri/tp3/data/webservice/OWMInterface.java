package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;


public interface OWMInterface {
    @Headers("Accept: application/geo+json")
    @GET("/data/2.5/weather?")

    public Call<WeatherResponse> getCurrentWeatherData(@Query("q") String query,
                                      @Query("APPID") String API_key);
}
