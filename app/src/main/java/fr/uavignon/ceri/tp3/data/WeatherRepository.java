package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;

    private CityDao cityDao;
    private final OWMInterface api;

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable = new MutableLiveData<Throwable>();

    private static volatile WeatherRepository INSTANCE;

    volatile int nbAPILoads = 0;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }


    public void loadWeatherAllCities(){
        List<City> cities = cityDao.getSynchrAllCities();
        nbAPILoads = cities.size();
        for (int i=0;i<cities.size();i++){
            loadWeatherCity(cities.get(i));
        }

    }



    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public void loadWeatherCity(City city) {
        isLoading.postValue(Boolean.TRUE);
        final String app_id = "1e289080e9256f259ba24e7aaa753993";
        String query = city.getName()+",,"+city.getCountryCode().toString();
        final MutableLiveData<WeatherResult> result = new MutableLiveData<>();

        api.getCurrentWeatherData(query, app_id).enqueue(
                new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call,
                                           Response<WeatherResponse> response) {
                        WeatherResult.transferInfo(response.body(), city);
                        updateCity(city);
                        if(nbAPILoads <= 1) isLoading.postValue(Boolean.FALSE);
                        else nbAPILoads--;
                        //result.postValue(new WeatherResult(false, response.body().properties.periods, null));
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        if(nbAPILoads <= 1) isLoading.postValue(Boolean.FALSE);
                        else nbAPILoads--;
                        webServiceThrowable.postValue(t);
                        //result.postValue(new WeatherResult(false, null, t));
                    }
                });

    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
